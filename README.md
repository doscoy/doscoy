# Pinned

## [filter player](https://gitlab.com/doscoy/filter-player)
wgslでフィルタを実装して画像、動画を加工するエディタ(chrome extensions)
- TypeScript, WebGPU, Svelte

## [gifenc.js](https://gitlab.com/doscoy/gifenc.js)
ブラウザでgif画像、アニメをエンコードするライブラリ
- TypeScript, WebAssembly

## [pixfont](https://gitlab.com/doscoy/pixfont)
グリフ画像からビットマップフォント、絵文字フォントを生成するコマンドラインツール
- c++20, TrueType

## [gbdev](https://gitlab.com/doscoy/gbdev)
ゲームボーイのソフト開発用アセンブラ
- c++17, WebAssembly

## [mod player](https://gitlab.com/doscoy/mod-player)
[soundtracker](https://en.wikipedia.org/wiki/Ultimate_Soundtracker)のmodファイルを再生するプレイヤー
- c++17

## [excel toys](https://gitlab.com/doscoy/excel-toys)
Excel VBAの悪ふざけ
